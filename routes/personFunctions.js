const fs = require("fs").promises

//functions
function getNextPersonKey(database){
    if (!database) throw new Error("I need a database!")
    return Math.max(...database.persons.map(p=>p.id)) + 1
}

function assignOthers(database){
    if (!database) throw new Error("I need a database!")
    let ids = database.persons.map(p=>p.id)
    let otherids = []
    while (ids.length>0) {
        let randomIndex = Math.floor(Math.random()*ids.length)
        otherids.push(ids.splice(randomIndex, 1)[0])
    }
    database.persons.forEach((p,i)=>p.otherid = otherids[i])
}

function convertNames(database, convertor) {
    database.persons.forEach(p=>p.name = convertor(p.name))
}

async function writeDBToFile(database){
    try {
        await fs.writeFile("./data/db.json", JSON.stringify(database))
    } catch (e) {
        console.error(e);
    }
}

module.exports = {getNextPersonKey, assignOthers, writeDBToFile, convertNames}
