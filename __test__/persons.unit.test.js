const persons = require("../routes/personFunctions")


function createTestDB() {
    return {
        persons: [{id: 1, name: "Jef", otherid: 2}, {id: 2, name: "Dirk", otherid: 3}, {
            id: 3,
            name: "Marie",
            otherid: 1
        }]
    }
}

describe("testing persons functions", () => {
    let database;
    beforeEach(() => {
        database = createTestDB();
    })
    describe("testing function getNextPersonKey", () => {
        it("should return one higher than highest index", () => {
            expect(persons.getNextPersonKey(database)).toBe(4)
        })
        it("should throw an error when no database provided", () => {
            expect(() => {
                persons.getNextPersonKey()
            }).toThrowError()
        })
    })
    describe("testing function assignOthers", () => {
        it("otherid should be different from my id", () => {
            //for (let i = 0; i < 10; i++) {
            //persons.assignOthers(database)
            database.persons.forEach(person => {
                expect(person.id).not.toBe(person.otherid)
            })
            //}
        })
    })
    describe("testing function convertNames", () => {
        const mockConvertor = jest.fn(name => name.toUpperCase())
        it("should call the convertor on all persons", () => {
            persons.convertNames(database, mockConvertor)
            expect(mockConvertor).toHaveBeenCalledTimes(3)
        })
    })
})